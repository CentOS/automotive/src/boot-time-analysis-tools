# Boot Timings

The `boot_timings` script collects boot time log information from dbus, dmesg, and systemd. The logs are normalized and output as a `_logs.json` file. The normalized logs are also fed into the `boot_timings.chart` module, which creates an HTML/JavaScript interactive cart output as a `_chart.html` file. Important boot time summary data that may be futher processed by automation and change detection systems is output as a `_summary.json` file. Finally, the systemd boot time data reported by dbus is structured into a hierarchical human-readable format and output to stdout.

## Python dependencies

Packages easily installed with `pip`
  - `bokeh`

These packages involve binary builds, and are easiest to install as RPMs
  - `dbus` (RPM package `python3-dbus`)
  - `systemd` (RPM package `python3-systemd`)

### Installing dependencies

Installing specific Python dependencies from the operating system's repositories rather than `pip` avoids pulling additional dependencies.

- Using the system's dependencies:
```
sudo dnf install -y python3-pip python3-systemd python3-dbus
```
- Using Pip dependencies:
```
sudo dnf install -y dbus-devel systemd-devel dbus-daemon
```
- Then,
```
git clone https://gitlab.com/CentOS/automotive/src/boot-time-analysis-tools.git
python3 -m pip install 'boot-time-analysis-tools/'
python3 -m pip install 'boot-time-analysis-tools/[graph]' # with the graph tools
```

## Usage

Running the `boot_timings` script with no parameters will use a default set of KPIs for timestamps and will output all generated files into the local directory. Some parameters are available to change script behaviors:

```
$ boot_timings --help
usage: boot_timings [-h] [-o OUTPUT_DIRECTORY] [-d DESCRIPTION] [--kpi-re-pattern KPI_RE_PATTERN] [--max-time MAX_TIME] [--offset OFFSET]

Collects logs and timings relevant to boot time performance.

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT_DIRECTORY, --output-directory OUTPUT_DIRECTORY
                        Directory where output files will be stored (default: .)
  -d DESCRIPTION, --description DESCRIPTION
                        Free form description to be added to the metadata (default: None)
  --kpi-re-pattern KPI_RE_PATTERN
                        Regular expression to match log messages on for KPI highlighting (default: multi-user.target|sysinit.target|Load Kernel Modules|mounted)
  --max-time MAX_TIME   Time in ms to limit the collection. Default to completion of switchroot. 0 to not limit collection. (default: )
  --offset OFFSET       Time in ms to offset SystemInit to measure wall clock time. Default to 0. (default: 0)
```

# Outputs

## Wall clock times

The script leverages [cntvct-log](../cntvct-log/) to determine an offset value of the kernel time to the wall clock time. Timestamps reported in stdout, the logs output file, and the chart ouput file are all adjusted to wall clock time using this offset as long as the `cntvct-log` service is reporting those wall clock times to the journal. If the `cntvct-log` service is not available, the script will display a warning and the timestamps will be reported based on kernel time instead.

*Note: This script only requires a post-switchroot deployment of `cntvct-log` with the hook for `basic.target`; it does not require `cntvct-log` to be deployed in the initramfs.*

## System initialization

The reported *SystemInit* boot stage is calculated from true boot time 0 (essentially power on) to kernel time 0. This means that this stage includes all firmware time as well as time spent by the Linux kernel prior to the start of the kernel clock. **Note that this includes the memory initialization phase of the kernel.**

### Addiitonal offset

Depending on the platform, additional offsets may be required due to hardware or firmware delays that occur outside the Linux processor to capture wall clock time. This extra offset can be specified in milliseconds using the `--offset` option.

## Metadata

Various metadata are collected from the system and are included in the output files. A free form text description can be added by the user to the metadata by passing a string to the `--description` flag.

## Timeline

The Timeline is an ASCII representation of the boot stages with relative sizes. The timeline will scale to the width of the console, or will default to 100 characters if output is redirected to a file. Each boot stage is labeled with its duration.

## Durations

The Durations are a table representation of key boot stages with start and end times. The columns include a tab character separator, allowing for direct copy-paste into a spreadsheet.

## Summary

The Summary shows key boot stage durations and KPIs in a hierarchical layout.

## Key Performance Indicator (KPI) Timestamps

KPIs report a table of timestamps of specific logged actions during the boot process. Like the Durations, the table is copy-paste friendly for spreadsheets.

The *First Initrd Service* and *First Root Service* KPIs are built-in and will always be shown. The other KPIs are based on a regular expression pattern match of the boot logs. The default regular expression (available from the `--help` output):

  > `multi-user.target|sysinit.target|Load Kernel Modules|mounted`

The pattern match can be modified by the user at run time with a valid regular expression string passed to the `--kpi-re-pattern` flag, which will result in overriding the default pattern above.

Example:

```
# boot_timings --kpi-re-pattern "ttyS[0-1]\."
...
## KPI Timestamps (ms)
KPI                                               	Timestamp
First Initrd Service Started At 20.958675 ms      	20958.675
First Root Service Started At 23.145611 ms        	23145.611
sys-devices-platform-serial8250-tty-ttyS0.dev…    	21533.939
dev-ttyS0.device                                  	21533.942
sys-devices-platform-serial8250-tty-ttyS1.dev…    	21534.463
dev-ttyS1.device                                  	21534.464
...
```

KPIs also affect the `_logs.json` and the `_chart.html` output files. Logs matching the pattern will have a `"kpi": "True"` value added to them in the `_logs.json` file, and will be highlighted in the graphical `_chart.html` file.

## Artifacts

Output files are generated by the script in order to support multiple use cases of data analysis.

  - `_output.log` - The human console output written to a file
  - `_summary.json` - A machine-readable summary of important durations and timestamps in milliseconds
  - `_log.json` - A machine-readable list of all captured logs and their timing information
  - `_chart.html` - A self-contained and interactive HTML/JavaScript chart of all captured logs plotted with their start times and durations

# Example
```
#  boot_timings --offset 1437


NOTE:
======
- A user specified offset of 1437.0ms have been applied to SystemInit.
- CNTVCT offset of 1316728ms has been applied SystemInit.
- Offset(s) applied. Times will be reported with an offset of 2753.728ms


## Timeline
 SystemInit: 2754ms
         Kernel: 858ms
           InitRD: 7626ms        Switchroot: 22097ms
|=======/=/=====================/=============================================================/=>


## Durations (ms)
Stage               Components          	Start               	End
SystemInit                              	0.0                 	2753.728
                    UserSpecifiedOffset 	0.0                 	1437.0
                    SystemInitCNTVCT    	1437.0              	2753.728
Kernel                                  	2753.728            	3611.761
InitRD                                  	3611.761            	11237.574
                    InitRDSystemdInit   	3611.761            	3755.028
                    InitRDSecurity      	3617.194            	3617.788
                    InitRDGenerators    	3727.614            	3727.985
                    InitRDUnitsLoad     	3727.995            	3755.028
Switchroot                              	11237.574           	33334.146
                    SystemdInit         	11237.574           	14065.434
                    Security            	11273.517           	12020.889
                    Generators          	12656.027           	12849.609
                    UnitsLoad           	12849.646           	14065.434


## Summary
SystemInit: 2753.728 ms
  UserSpecifiedOffset: 1437.0 ms
  SystemInitCNTVCT: 1316.728 ms
Kernel: 858.033 ms
InitRD: 7625.813 ms
  InitRDSystemdInit: 143.267 ms
    InitRDSecurity: 0.594 ms
    InitRDGenerators: 0.371 ms
    InitRDUnitsLoad: 27.033 ms
  First Initrd Service Started At 3.755028 ms
Switchroot: 22096.572 ms
  SystemdInit: 2827.86 ms
    Security: 747.372 ms
    Generators: 193.582 ms
    UnitsLoad: 1215.788 ms
  First Root Service Started At 14.065434 ms
Overall: 33334.146 ms
NInstalledJobs: 157
NFailedJobs: 0.0 ms


## KPI Timestamps (ms)
KPI                                               	Timestamp
First Initrd Service Started At 3.755028 ms       	3755.028
First Root Service Started At 14.065434 ms        	14065.434
systemd[1]: Load Kernel Modules was skipped b…    	3807.79
kernel: EXT4-fs (mmcblk0p5): mounted filesyst…    	4204.593
kernel: erofs: (device loop0): mounted with r…    	4292.706
systemd[1]: Starting Load Kernel Modules...       	14490.546
systemd[1]: Finished Load Kernel Modules.         	14602.556
sysinit.target                                    	19291.615
multi-user.target                                 	29645.163

```
