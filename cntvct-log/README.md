# cntvct logger

A trivial binary to dump the content of cntvct from CLI in human readable
units. Sub-packages provide a Systemd service to hook to Systemd targets and a
dracut module lets one do just that from the initramfs.

This comes with all the caveat associated with _cntvct_.

### Building from sources (aarch64 only)

```
$ git clone https://gitlab.com/CentOS/automotive/src/boot-time-analysis-tools.git
$ cd boot-time-analysis-tools/cntvct-log
$ meson setup builddir
$ cd builddir
$ meson compile
```

## Usage

To get cntvct logs after switchroot for `basic.service`:
```
$ sudo systemctl enable cntvct@basic.service # enable the log when basic.target is reached.
```
In order to get cntvct logs during the initramfs phase, the `-dracut` package will need to be installed above, and your initramfs image will need to be updated.
```
$ sudo dracut --add cntvct --show-modules <path-to-initramfs> # This will register logs at local-fs, basic, initrd and initrd-switch-root targets. The dracut module can be modified to change the behavior.
```
The logs will be present in the boot journal.
```
$ journalctl -b0 -o short-monotonic | grep 'cntvct@.*[0-9.]s\.'
[    0.582683] localhost cntvct@local-fs[194]: 5.409352s.
[    0.885672] localhost cntvct@basic[228]: 5.768381s.
[    2.022430] localhost cntvct@initrd[276]: 6.905183s.
[   20.222714] localhost cntvct@initrd-switch-root[345]: 25.105452s.
```
