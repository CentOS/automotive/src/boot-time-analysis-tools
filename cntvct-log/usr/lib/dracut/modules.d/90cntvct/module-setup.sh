#! /usr/bin/bash

# Prerequisite check(s) for module.
check() {
	# Return 255 to only include the module, if another module requires it.
	return 255
}

# Module dependency requirements.
depends() {
	# Return 0 to include the dependent module(s) in the initramfs.
	return 0
}

# Install the required file(s) and directories for the module in the initramfs.
install() {
	inst_multiple -o \
		"$systemdsystemunitdir"/cntvct@.service \
		/usr/bin/cntvct

	"$SYSTEMCTL" -q --root "$initdir" enable cntvct@local-fs.service
	"$SYSTEMCTL" -q --root "$initdir" enable cntvct@basic.service
	"$SYSTEMCTL" -q --root "$initdir" enable cntvct@initrd.service
	"$SYSTEMCTL" -q --root "$initdir" enable cntvct@initrd-switch-root.service
}
