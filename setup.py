from setuptools import setup

setup(
    name='boot-time-analysis',
    version='0.5',
    install_requires=[
        'dbus-python',
        'systemd-python',
    ],
    extras_require={
        'graph': ['bokeh'],
    },
    entry_points={
        'console_scripts': [
            'boot_timings = boot_timings.cli:main',
        ],
    },
    package_dir={'': 'src'},
    packages=['boot_timings'],
)
