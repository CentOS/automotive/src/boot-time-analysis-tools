# Boot Time Analysis Tools

A collection of tools for analyzing boot times on aarch64 platforms.

## [cntvct-log](cntvct-log)
A binary tool deployed as systemd hooks to log wall clock time stamps from power on.

## [boot_timings](src/boot_timings)
A python package to collect boot time log information from dbus, dmesg, and systemd-analyze and provide summary data in various formats.

## Installation

This tool is now available in the [CentOS Automotive SIG testing stream](https://cbs.centos.org/koji/packageinfo?packageID=11332).

- Add the CentOS Automotive SIG testing stream:
```
# cat - >> /etc/yum.repos.d/CentOS-Stream-Automotive.repo <<EOF
[centos-automotive-testing]
name=CentOS Stream $releasever - Automotive - Testing
baseurl=https://buildlogs.centos.org/$stream/automotive/$basearch/packages-main/
gpgcheck=1
priority=50
enabled=1
skip_if_unavailable=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-Automotive
EOF
```
- Install the package:
```
# dnf install boot-time-analysis-tools
```
